package movies.importer;

import java.util.ArrayList;

public class LowercaseProcessor extends Processor{
	public LowercaseProcessor(String sourceDir, String destinationDir) {
		super(sourceDir,destinationDir,true);
	}
	@Override
	public ArrayList<String> process(ArrayList<String> s){
		ArrayList<String> asLower=new ArrayList<String>();
		for(int i=0;i<s.size();i++)
			asLower.add(s.get(i).toLowerCase());
		return asLower;
	}
}
