package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor{
	public RemoveDuplicates(String sourceDir, String destinationDir) {
		super(sourceDir,destinationDir,false);
	}
	@Override
	public ArrayList<String> process(ArrayList<String> s){
		ArrayList<String> noDupes=new ArrayList<String>();
		for(int i=0;i<s.size();i++) {
			if(!noDupes.contains(s.get(i)))
				noDupes.add(s.get(i));
		}
		return noDupes;
	}
}
